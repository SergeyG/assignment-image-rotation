#include "../image/image.h"
#include "../utilities/operations.h"
#include "../utilities/utils.h"
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

static enum operation_status read_header(FILE* const f, struct bmp_header* const header) {

    if (fread(header, sizeof(struct bmp_header), 1, f) != 1)
        return READ_ERROR;
    
    return SUCCESS;
    
}

static enum operation_status read_pixels(FILE* const f, struct image* const img) {

    const uint32_t width = img->width;
    const uint32_t height = img->height;
    
    const uint8_t padding = get_padding(width);
    
    for (size_t i = 0; i < height; ++i) {
        if (fread(img->data + i * width, sizeof(struct pixel), width, f ) != width)
            return READ_ERROR;

        if (fseek(f, padding, SEEK_CUR) != 0)
            return SEEK_ERROR;
    }
    return SUCCESS;
}

static enum operation_status read_image(FILE* const f, const struct bmp_header* const header, struct image* const img) {

    *img = create_image(header->width, header->height);

    if (img->data == NULL) {
        free_image(*img);
        return ALLOCATION_ERROR;
    }

    if (fseek(f, header->offset, SEEK_SET) != 0) {
        free_image(*img);
        return SEEK_ERROR;
    }

    return read_pixels(f, img);
}

enum operation_status from_bmp(FILE* const f, struct image* const img) {
    struct bmp_header header = {0};

    enum operation_status status = read_header(f, &header);
    if (status != SUCCESS) return status;

    return read_image(f, &header, img);
}
