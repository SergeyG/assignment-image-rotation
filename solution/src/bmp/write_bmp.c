#include "../image/image.h"
#include "../utilities/operations.h"
#include "../utilities/utils.h"
#include "bmp.h"
#include <stdio.h>

static const struct bmp_header new_header = {
    .signature = 0x4D42,
    .reserved = 0,
    .offset = sizeof(struct bmp_header),
    .header_size = 40,
    .planes = 1,
    .bit_count = 24,
    .compression = 0,
    .ppmx = 2834,
    .ppmy = 2834,
    .clr_used = 0,
    .clr_imoprtant = 0
};

static uint32_t calculate_image_size(const uint64_t width, const uint64_t height) {
    const uint8_t padding = get_padding(width);
    return (width * sizeof(struct pixel) + padding) * height;
}

static uint32_t calculate_file_size(const uint32_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

static struct bmp_header create_header(const struct image* const img) {
    struct bmp_header header = new_header;
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    header.width = width;
    header.height = height;

    header.image_size = calculate_image_size(width, height);
    header.file_size = calculate_file_size(header.image_size);

    return header;
} 

static enum operation_status write_header(FILE* f, const struct bmp_header* const header) {
    if (fseek(f, 0, SEEK_SET) != 0)
        return SEEK_ERROR;
    if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1)
        return WRITE_ERROR;
    return SUCCESS;
}

static enum operation_status write_image(FILE* const f, const struct image* const img) {
    if (fseek(f, sizeof(struct bmp_header), SEEK_SET) != 0)
        return SEEK_ERROR;

    const uint8_t padding = get_padding(img->width);
    
    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, f) != img->width)
            return WRITE_ERROR;
        if (fseek(f, padding, SEEK_CUR) != 0)
            return SEEK_ERROR;
    }
    return SUCCESS;
}

enum operation_status to_bmp(FILE* const f, const struct image* const img) {
    const struct bmp_header header = create_header(img);
    enum operation_status status = write_header(f, &header);
    if (status != SUCCESS)
        return status;
    return write_image(f, img);
}
