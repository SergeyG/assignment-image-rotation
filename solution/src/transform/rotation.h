#ifndef ROTATE_H
#define ROTATE_H

#include "../image/image.h"
#include "../utilities/operations.h"

enum operation_status rotate(struct image* const img);

#endif
