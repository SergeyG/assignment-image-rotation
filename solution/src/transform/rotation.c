#include "../image/image.h"
#include "../utilities/operations.h"
#include  <stdint.h>
#include <stdio.h>
#include <string.h>


static struct pixel get_pixel(const struct image* const img, const size_t row, const size_t col) {
    return img->data[row * img->width + col];
}

static void set_pixel(struct image* const img, const size_t row, const size_t col, const struct pixel pixel) {
    img->data[row * img->width + col] = pixel;
}

enum operation_status rotate(struct image* const img) {
    const uint32_t width = img->width;
    const uint32_t height = img->height;

    struct image new_img = create_image(height, width);

    if (new_img.data == NULL) return ALLOCATION_ERROR;

    for (size_t i = 0; i < height; ++i) {
        for(size_t j = 0; j < width; ++j) {
            const struct pixel pixel = get_pixel(img, i, j);
            set_pixel(&new_img, j, height - i - 1, pixel);
        }
    }

    free_image(*img);
    *img = new_img;

    return SUCCESS;

}
