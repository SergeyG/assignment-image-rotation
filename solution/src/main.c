#include "file_operations/file_work.h"
#include "transform/rotation.h"
#include <stdio.h>
#include <stdlib.h>


void handle_error(enum operation_status status, struct image img);
_Noreturn void err(const char *msg, ...);

int main(int argc, char* argv[]) {

    if (argc < 3) {
        err("Not enough arguments\n");
    }

    const char* const in_filename = argv[1];
    const char* const out_filename = argv[2];

    struct image img = {0};

    enum operation_status status = read_bmp_file(in_filename, &img);

    if (status != SUCCESS) {
        handle_error(status, img);
    }

    status = rotate(&img);
    if (status != SUCCESS) {
        handle_error(status, img);
    } 

    status = write_bmp_file(out_filename, &img);
    if (status != SUCCESS) {
        handle_error(status, img);
    }

    free_image(img);

    return 0;
}

