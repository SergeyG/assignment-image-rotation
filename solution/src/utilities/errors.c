#include "../image/image.h"
#include "operations.h"
#include "stdio.h"
#include <stdlib.h>

static char* error_messages[] = {
    [SUCCESS] = "Success",
    [READ_ERROR] = "Error, while reading a file",
    [OPEN_ERROR] = "Unable to open the file",
    [SEEK_ERROR] = "Error, while working with file",
    [WRITE_ERROR] = "Error, while writing to a file",
    [CLOSE_ERROR] = "Unable to close the file",
    [ALLOCATION_ERROR] = "Unable to allocate memory"
};

void handle_error(const enum operation_status status, const struct image img) {
    free_image(img);
    fprintf(stderr, "%s\n", error_messages[status]);
    abort();
}


_Noreturn void err(const char *msg, ...) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}


